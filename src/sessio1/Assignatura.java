package sessio1;


/**
 * The Class Assignatura.
 */
public class Assignatura {

	/** The nom assignatura. */
	private String nomAssignatura;

	/** The codi assignatura. */
	private final String codiAssignatura;

	/** The credits teorics. */
	private int creditsTeorics;

	/** The credits practics. */
	private int creditsPractics;

	/**
	 * Instantiates a new assignatura.
	 *
	 * @param nomAssignatura the nom assignatura
	 * @param codiAssignatura the codi assignatura
	 * @param creditsTeorics the credits teorics
	 * @param creditsPractics the credits practics
	 */
	public Assignatura(String nomAssignatura, String codiAssignatura, int creditsTeorics, int creditsPractics) {
		this.nomAssignatura = nomAssignatura;
		this.codiAssignatura = codiAssignatura;
		this.creditsTeorics = creditsTeorics;
		this.creditsPractics = creditsPractics;
	}

	/**
	 * Gets the nom assignatura.
	 *
	 * @return the nomAssignatura
	 */
	public String getNomAssignatura() {
		return nomAssignatura;
	}

	/**
	 * Sets the nom assignatura.
	 *
	 * @param nomAssignatura the nomAssignatura to set
	 */
	public void setNomAssignatura(String nomAssignatura) {
		this.nomAssignatura = nomAssignatura;
	}

	/**
	 * Gets the credits teorics.
	 *
	 * @return the creditsTeorics
	 */
	public int getCreditsTeorics() {
		return creditsTeorics;
	}

	/**
	 * Sets the credits teorics.
	 *
	 * @param creditsTeorics the creditsTeorics to set
	 */
	public void setCreditsTeorics(int creditsTeorics) {
		if (creditsTeorics>0) {
			this.creditsTeorics = creditsTeorics;
		}

	}

	/**
	 * Gets the credits practics.
	 *
	 * @return the creditsPractics
	 */
	public int getCreditsPractics() {
		return creditsPractics;
	}

	/**
	 * Sets the credits practics.
	 *
	 * @param creditsPractics the creditsPractics to set
	 */
	public void setCreditsPractics(int creditsPractics) {
		if (creditsPractics>0) {
			this.creditsPractics = creditsPractics;	
		}

	}

	/**
	 * Gets the codi assignatura.
	 *
	 * @return the codiAssignatura
	 */
	public String getCodiAssignatura() {
		return codiAssignatura;
	}

	/**
	 * Gets the total.
	 *
	 * @return the total
	 */
	public float getTotal() {
		float total=0;
		total = creditsPractics+creditsTeorics;
		return total;

	}

	/**
	 * Percentatge.
	 *
	 * @param teorics the teorics
	 * @return the float
	 */
	public float percentatge(boolean teorics) {
		float percentatge=0;
		float creditsTotals=getTotal();
		float credits=0;
		if (teorics) {
			credits=creditsTeorics;
		} else {
			credits=creditsPractics;
		}
		percentatge=(credits/creditsTotals)*100;
		return percentatge;
	}

	/**
	 * Puja credits.
	 *
	 * @param num the num
	 * @param teorics the teorics
	 */
	public void pujaCredits (int num, boolean teorics) {
		if (teorics) {
			if (creditsTeorics+num>0) {
				creditsTeorics=+num;
			}
		} else {
			if (creditsPractics+num>0) {
				creditsPractics=+num;	
			}
		}
	}

	/**
	 * Baixa credits.
	 *
	 * @param num the num
	 * @param teorics the teorics
	 */
	public void baixaCredits (int num, boolean teorics) {
		pujaCredits(-(num), teorics);
	}

	/**
	 * Gets the curs.
	 *
	 * @return the curs
	 */
	public String getCurs() {

		String curs= codiAssignatura.substring(4, 6);
		String c="";
		switch (curs) {
		case "01":
			c= "primer";
			break;
		case "02":
			c= "segon";
			break;
		case "03":
			c= "tercer";
			break;
		case "04":
			c= "quart";
			break;			
		}
		return c;
	}

	/**
	 * Gets the materia.
	 *
	 * @return the materia
	 */
	public  String getMateria() {
		return codiAssignatura.substring(7, 9);
	}

	/**
	 * Gets the codi.
	 *
	 * @return the codi
	 */
	public String getCodi() {
		return codiAssignatura.substring(0, 3);
	}

	/**
	 * Gets the tipus.
	 *
	 * @return the tipus
	 */
	public String getTipus() {
		String tipus= codiAssignatura.substring(10,11);
		String t;
		if (tipus.equals("O")) {
			t="optativa";
		}else if (tipus.equals("B")) {
			t="basica";
		}else {
			t= "troncal";
		}
		return t;
	}



	public String visualitza() {

		return "Assignatura [nomAssignatura=" + nomAssignatura + 
				",\n codiAssignatura=" + codiAssignatura+
				"\n "+getCodi()+" de "+getCurs()+" dins la materia "+ getMateria()+" i "+getTipus()
				+ ",\n creditsTeorics=" + creditsTeorics + " credits que representen un "+percentatge(true)+","+" \n creditsPractics=" + creditsPractics +" credits que representen un "+percentatge(false)+ "]";
	}



}
