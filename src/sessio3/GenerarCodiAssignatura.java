package sessio3;

import java.util.Random;

/**
 * The Class GenerarCodiAssignatura.
 */
public class GenerarCodiAssignatura{
	
	 /**
 	 * Generar codi.
 	 *
 	 * @return the string
 	 */
 	public static String generarCodi() {
	 char[] id = new char[11];
	 id[0]=generarDigit();
	 id[1]=generarDigit();
	 id[2]=generarDigit();
	 id[3]='-';
	 id[4]='0';
	 char d = 0;
	 while (!((d=='1')|(d=='2')|(d=='3')|(d=='4'))) {
		d=generarDigit();
	}
	 
	 id[5]=d;
	 id[6]='-';
	 id[7]=generarLletra();
	 id[8]=generarLletra();
	 id[9]='-';
	 char l = 0;
	 while (!((l=='O')|(l=='B')|(l=='T'))) {
		l=generarLletra();
	}
	 id[10]=l;
	 String s = new String(id);
	return s;
	 }
	 
 	/**
 	 * Generar codi.
 	 *
 	 * @param identificador the identificador
 	 * @return the string
 	 */
 	public static String generarCodi(String identificador) {
	 char[] id = new char[11];
	 char[] is= identificador.toCharArray();
	 id[0]=is[0];
	 id[1]=is[1];
	 id[2]=is[2];
	 id[3]='-';
	 id[4]='0';
	 char d = 0;
	 while (!((d=='1')|(d=='2')|(d=='3')|(d=='4'))) {
		d=generarDigit();
	}
	 
	 id[5]=d;
	 id[6]='-';
	 id[7]=generarLletra();
	 id[8]=generarLletra();
	 id[9]='-';
	 char l = 0;
	 while (!((l=='O')|(l=='B')|(l=='T'))) {
		l=generarLletra();
	}
	 id[10]=l;
	 String s = new String(id);
	return s;
	 }
	 
 	/**
 	 * Generar codi.
 	 *
 	 * @param tipus the tipus
 	 * @return the string
 	 */
 	public static String generarCodi(char tipus) {
		 char[] id = new char[11];
		 id[0]=generarDigit();
		 id[1]=generarDigit();
		 id[2]=generarDigit();
		 id[3]='-';
		 id[4]='0';
		 char d = 0;
		 while (!((d=='1')|(d=='2')|(d=='3')|(d=='4'))) {
			d=generarDigit();
		}
		 
		 id[5]=d;
		 id[6]='-';
		 id[7]=generarLletra();
		 id[8]=generarLletra();
		 id[9]='-';
		 id[10]=tipus;
		 String s = new String(id);
		return s;
	 }
	 
 	/**
 	 * Generar digit.
 	 *
 	 * @return the char
 	 */
 	private static char generarDigit() {
		 Random r = new Random();
		 int low = 48;
		 int high = 57;
		 int result = r.nextInt((high-low)+1) + low;
		 char c = (char) result;
		 return c;
	 }
	 
 	/**
 	 * Generar lletra.
 	 *
 	 * @return the char
 	 */
 	private static char generarLletra() {
		 Random r = new Random();
		 int low = 65;
		 int high = 90;
		 int result = r.nextInt((high-low)+1) + low;
		 char c = (char) result;
		 return c;
	 }
	}