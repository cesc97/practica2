package sessio3;

/**
 * The Class Reconeguda.
 */
public class Reconeguda extends Optativa {

/** The cicle superior. */
private boolean cicleSuperior;

/** The nom titulacio. */
private String nomTitulacio;

/**
 * Instantiates a new reconeguda.
 *
 * @param nomAssignatura the nom assignatura
 * @param codiAssignatura the codi assignatura
 * @param creditsTeorics the credits teorics
 * @param creditsPractics the credits practics
 * @param cicleSuperior the cicle superior
 * @param nomTitulacio the nom titulacio
 */
public Reconeguda(String nomAssignatura, String codiAssignatura, int creditsTeorics, int creditsPractics,
		boolean cicleSuperior, String nomTitulacio) {
	super(nomAssignatura, codiAssignatura, creditsTeorics, creditsPractics);
	this.cicleSuperior = cicleSuperior;
	this.nomTitulacio = nomTitulacio;
}

/**
 * Instantiates a new reconeguda.
 *
 * @param nomAssignatura the nom assignatura
 * @param codiAssignatura the codi assignatura
 * @param creditsTeorics the credits teorics
 * @param creditsPractics the credits practics
 * @param nomTitulacio the nom titulacio
 */
public Reconeguda(String nomAssignatura, String codiAssignatura, int creditsTeorics, int creditsPractics, String nomTitulacio) {
	this(nomAssignatura, codiAssignatura, creditsTeorics, creditsPractics, false, nomTitulacio);
	}

/**
 * Checks if is cicle superior.
 *
 * @return the cicleSuperior
 */
public boolean isCicleSuperior() {
	return cicleSuperior;
}

/**
 * Sets the cicle superior.
 *
 * @param cicleSuperior the cicleSuperior to set
 */
public void setCicleSuperior(boolean cicleSuperior) {
	this.cicleSuperior = cicleSuperior;
}

/**
 * Gets the nom titulacio.
 *
 * @return the nomTitulacio
 */
public String getNomTitulacio() {
	return nomTitulacio;
}

/**
 * Sets the nom titulacio.
 *
 * @param nomTitulacio the nomTitulacio to set
 */
public void setNomTitulacio(String nomTitulacio) {
	this.nomTitulacio = nomTitulacio;
}

public String visualitza() {
	return super.visualitza()+"\n titulacio: "+nomTitulacio+" cicle superior: "+cicleSuperior;
}

public String visualitza(boolean percentatge) {
	return super.visualitza(percentatge)+"\n titulacio: "+nomTitulacio+" cicle superior: "+cicleSuperior;
}

}
