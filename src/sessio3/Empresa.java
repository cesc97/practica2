package sessio3;

/**
 * The Class Empresa.
 */
public class Empresa extends Optativa{

/** The numero conveni. */
private int numeroConveni;

/** The hores renumerades. */
private int horesRenumerades;

/** The curricular. */
private boolean curricular;

/**
 * Instantiates a new empresa.
 *
 * @param nomAssignatura the nom assignatura
 * @param codiAssignatura the codi assignatura
 * @param creditsTeorics the credits teorics
 * @param creditsPractics the credits practics
 * @param numeroConveni the numero conveni
 * @param horesRenumerades the hores renumerades
 * @param curricular the curricular
 */
public Empresa(String nomAssignatura, String codiAssignatura, int creditsTeorics, int creditsPractics,
		int numeroConveni, int horesRenumerades, boolean curricular) {
	super(nomAssignatura, codiAssignatura, creditsTeorics, creditsPractics);
	this.numeroConveni = numeroConveni;
	this.horesRenumerades = horesRenumerades;
	this.curricular = curricular;
}

/**
 * Instantiates a new empresa.
 *
 * @param nomAssignatura the nom assignatura
 * @param codiAssignatura the codi assignatura
 * @param creditsTeorics the credits teorics
 * @param creditsPractics the credits practics
 * @param numeroConveni the numero conveni
 */
public Empresa(String nomAssignatura, String codiAssignatura, int creditsTeorics, int creditsPractics,
		int numeroConveni) {
	this( nomAssignatura,  codiAssignatura,  creditsTeorics,  creditsPractics, numeroConveni,  0,  false);
}

/**
 * Instantiates a new empresa.
 *
 * @param nomAssignatura the nom assignatura
 * @param codiAssignatura the codi assignatura
 * @param creditsTeorics the credits teorics
 * @param creditsPractics the credits practics
 * @param numeroConveni the numero conveni
 * @param horesRenumerades the hores renumerades
 */
public Empresa(String nomAssignatura, String codiAssignatura, int creditsTeorics, int creditsPractics,
		int numeroConveni, int horesRenumerades) {
	this( nomAssignatura,  codiAssignatura,  creditsTeorics,  creditsPractics, numeroConveni,horesRenumerades,false);
}

/**
 * Instantiates a new empresa.
 *
 * @param nomAssignatura the nom assignatura
 * @param codiAssignatura the codi assignatura
 * @param creditsTeorics the credits teorics
 * @param creditsPractics the credits practics
 * @param numeroConveni the numero conveni
 * @param curricular the curricular
 */
public Empresa(String nomAssignatura, String codiAssignatura, int creditsTeorics, int creditsPractics,
		int numeroConveni, boolean curricular) {
	this( nomAssignatura,  codiAssignatura,  creditsTeorics,  creditsPractics, numeroConveni,0,false);
}

/**
 * Gets the numero conveni.
 *
 * @return the numeroConveni
 */
public int getNumeroConveni() {
	return numeroConveni;
}

/**
 * Sets the numero conveni.
 *
 * @param numeroConveni the numeroConveni to set
 */
public void setNumeroConveni(int numeroConveni) {
	this.numeroConveni = numeroConveni;
}

/**
 * Gets the hores renumerades.
 *
 * @return the horesRenumerades
 */
public int getHoresRenumerades() {
	return horesRenumerades;
}

/**
 * Sets the hores renumerades.
 *
 * @param horesRenumerades the horesRenumerades to set
 */
public void setHoresRenumerades(int horesRenumerades) {
	this.horesRenumerades = horesRenumerades;
}

/**
 * Checks if is curricular.
 *
 * @return the curricular
 */
public boolean isCurricular() {
	return curricular;
}

/**
 * Sets the curricular.
 *
 * @param curricular the curricular to set
 */
public void setCurricular(boolean curricular) {
	this.curricular = curricular;
}


public String visualitza() {
	return super.visualitza()+"\n numero conveni: "+numeroConveni+" hores renumerades: "+horesRenumerades+" curricular: "+curricular;
}

public String visualitza(boolean percentatge) {
	return super.visualitza(percentatge)+"\n numero conveni: "+numeroConveni+" hores renumerades: "+horesRenumerades+" curricular: "+curricular;
}
}
