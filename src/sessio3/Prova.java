package sessio3;


/**
 * The Class Prova.
 */
public class Prova {

	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {
		Assignatura ass1= new Obligatoria("intro bd", "342-01-BD-B", 2, 1, false, "bases de dades");
		Assignatura ass2= new Troncal("prog poo" , "674-01-PR-B", 3, 2, "programacio");
		Assignatura ass3= new Reconeguda("web" , "936-01-WB-T", 4, 3 ,true, "DAW");
		Assignatura ass4= new Reglada("iot" , "234-01-DV-O", 3,2, "angles", "IOT");
		Assignatura ass5 = new Empresa("sysadmin","453-04-AX-O", 4, 5, 0763, 30, true);


		System.out.println("metodes grau");
		Grau g = new Grau("informatica", 60, 6, 4);
		g.addAssignatura(ass1);
		g.addAssignatura(ass2);
		g.addAssignatura(ass3);
		g.addAssignatura(ass4);
		g.addAssignatura(ass5);

		g.llistat();

	}

}
