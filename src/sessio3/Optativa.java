package sessio3;

/**
 * The Class Optativa.
 */
public abstract class Optativa extends Assignatura {

/** The reglada. */
protected static int reglada;

/** The reconeguda. */
protected static int reconeguda;

/** The empresa. */
protected static int empresa;

/**
 * Instantiates a new optativa.
 *
 * @param nomAssignatura the nom assignatura
 * @param codiAssignatura the codi assignatura
 * @param creditsTeorics the credits teorics
 * @param creditsPractics the credits practics
 */
public Optativa(String nomAssignatura,String codiAssignatura, int creditsTeorics, int creditsPractics) {
	super(nomAssignatura, codiAssignatura,creditsTeorics,creditsPractics);
	super.optativaCount++;
}

/**
 * Gets the reglada.
 *
 * @return the reglada
 */
public static int getReglada() {
	return reglada;
}

/**
 * Gets the reconeguda.
 *
 * @return the reconeguda
 */
public static int getReconeguda() {
	return reconeguda;
}

/**
 * Gets the empresa.
 *
 * @return the empresa
 */
public static int getEmpresa() {
	return empresa;
}
public String visualitza() {
	return super.visualitza();
}
public String visualitza(boolean percentatge) {
	return super.visualitza(percentatge);
}
}
