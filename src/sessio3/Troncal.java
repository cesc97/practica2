package sessio3;

import java.util.Date;

/**
 * The Class Troncal.
 *
 * @author Francesc
 */
public class Troncal extends Assignatura {
	
	/** The nom materia. */
	private String nomMateria;
	
	/** The any. */
	private Date any;
	
	/**
	 * Instantiates a new troncal.
	 *
	 * @param nomAssignatura the nom assignatura
	 * @param codiAssignatura the codi assignatura
	 * @param creditsTeorics the credits teorics
	 * @param creditsPractics the credits practics
	 * @param nomMateria the nom materia
	 * @param any the any
	 */
	public Troncal(String nomAssignatura, String codiAssignatura, int creditsTeorics, int creditsPractics,
			String nomMateria, Date any) {
		super(nomAssignatura, codiAssignatura, creditsTeorics, creditsPractics);
		this.nomMateria = nomMateria;
		this.any = any;
		super.troncalCount++;
	}
	
	/**
	 * Instantiates a new troncal.
	 *
	 * @param nomAssignatura the nom assignatura
	 * @param codiAssignatura the codi assignatura
	 * @param creditsTeorics the credits teorics
	 * @param creditsPractics the credits practics
	 * @param nomMateria the nom materia
	 */
	public Troncal(String nomAssignatura, String codiAssignatura, int creditsTeorics, int creditsPractics,
			String nomMateria) {
		this(nomAssignatura, codiAssignatura, creditsTeorics, creditsPractics, nomMateria, null);
		super.troncalCount++;
	}

	/**
	 * Nom materia.
	 *
	 * @return the nomMateria
	 */
	public String nomMateria() {
		return nomMateria;
	}

	/**
	 * Sets the nom materia.
	 *
	 * @param nomMateria the nomMateria to set
	 */
	public void setnomMateria(String nomMateria) {
		this.nomMateria = nomMateria;
	}

	/**
	 * Gets the any.
	 *
	 * @return the any
	 */
	public Date getAny() {
		return any;
	}

	/**
	 * Sets the any.
	 *
	 * @param any the any to set
	 */
	public void setAny(Date any) {
		this.any = any;
	}
	
	/**
	 * Visualitza.
	 *
	 * @return the string
	 */
	public String visualitza() {
		
		return super.visualitza()+"\n materia: "+nomMateria+" any: "+any;

	}
	
	/**
	 * Visualitza.
	 *
	 * @param percentatge the percentatge
	 * @return the string
	 */
	public String visualitza(boolean percentatge) {
		return super.visualitza(percentatge)+"\n materia: "+nomMateria+" any: "+any;
		
	}
	

}
