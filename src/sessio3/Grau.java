package sessio3;

/**
 * The Class Grau.
 */
public class Grau {

	/** The nom grau. */
	private String nomGrau;

	/** The numero credits. */
	private int numeroCredits;

	/** The obligatorietat. */
	private Assignatura [][] obligatorietat;

	/** The obligatorietat max 4. */
	private int obligatorietatMax4;

	/** The obligatorietat 4 a 6. */
	private int obligatorietat4a6;

	/** The obligatorietat min 6. */
	private int obligatorietatMin6;

	/** The optativitat. */
	private Assignatura [] optativitat;

	/** The numero assignatures optatives. */
	private int numeroAssignaturesOptatives;



	/**
	 * Instantiates a new grau.
	 *
	 * @param nomGrau the nom grau
	 * @param numeroCredits the numero credits
	 * @param numeroAssignaturesObligatories the numero assignatures obligatories
	 * @param numeroAssignaturesOptatives the numero assignatures optatives
	 */
	public Grau(String nomGrau, int numeroCredits,int numeroAssignaturesObligatories, int numeroAssignaturesOptatives) {
		this.nomGrau = nomGrau;
		this.numeroCredits = numeroCredits;
		this.obligatorietat = new Assignatura[3][numeroAssignaturesObligatories];
		this.numeroAssignaturesOptatives = 0;
		this.optativitat=new Assignatura[numeroAssignaturesOptatives];
		this.obligatorietat4a6=0;
		this.obligatorietatMax4=0;
		this.obligatorietatMin6=0;
	}
	
	/**
	 * Instantiates a new grau.
	 *
	 * @param nomGrau the nom grau
	 * @param numeroCredits the numero credits
	 * @param numeroAssignaturesObligatories the numero assignatures obligatories
	 * @param numeroAssignaturesOptatives the numero assignatures optatives
	 * @param assignatures the assignatures
	 */
	public Grau(String nomGrau, int numeroCredits,int numeroAssignaturesObligatories, int numeroAssignaturesOptatives,Assignatura[] assignatures) {
		this(nomGrau, numeroCredits, numeroAssignaturesObligatories, numeroAssignaturesOptatives);
		for (int i = 0; i < assignatures.length; i++) {
			this.addAssignatura(assignatures[i]);
		}
	}


	/**
	 * Gets the nom grau.
	 *
	 * @return the nomGrau
	 */

	public String getNomGrau() {
		return nomGrau;
	}

	/**
	 * Sets the nom grau.
	 *
	 * @param nomGrau the nomGrau to set
	 */
	public void setNomGrau(String nomGrau) {
		this.nomGrau = nomGrau;
	}

	/**
	 * Gets the numero credits.
	 *
	 * @return the numeroCredits
	 */
	public int getNumeroCredits() {
		return numeroCredits;
	}

	/**
	 * Sets the numero credits.
	 *
	 * @param numeroCredits the numeroCredits to set
	 */
	public void setNumeroCredits(int numeroCredits) {
		if (numeroCredits>0) {
			this.numeroCredits = numeroCredits;	
		}
		
	}

	/**
	 * Gets the numero assignatures optatives.
	 *
	 * @return the numeroAssignaturesOptatives
	 */
	public int getNumeroAssignaturesOptatives() {
		return numeroAssignaturesOptatives;
	}

	/**
	 * Adds the assignatura.
	 *
	 * @param a the a
	 */
	public void addAssignatura(Assignatura a){
		float numCredits= a.getTotal();
		if (!teAssignatura(a.getCodiAssignatura())) {
			if (a instanceof Optativa){
				optativitat[numeroAssignaturesOptatives]=a;
				numeroAssignaturesOptatives++;
			} else if ((a instanceof Obligatoria)||a instanceof Troncal){
				if (numCredits<4) {
					obligatorietat[0][obligatorietatMax4]=a;
					obligatorietatMax4++;
				} else if (numCredits>6) {
					obligatorietat[2][obligatorietatMin6]=a;
					obligatorietatMin6++;
				}else{
					obligatorietat[1][obligatorietat4a6]=a;
					obligatorietat4a6++;
				}
			}
		}else {
			System.out.println("el grau ja conte aquesta assignatura");
		}

	} 

	/**
	 * Rem assignatura.
	 *
	 * @param a the a
	 */
	public void remAssignatura (Assignatura a){
		float numCredits= a.getTotal();
		String codiAssignatura = a.getCodiAssignatura();
		if (a instanceof Optativa){
			for (int i = 0; i < numeroAssignaturesOptatives; i++) {
				if (optativitat[i].getCodiAssignatura().equals(codiAssignatura)) {
					optativitat[i]=optativitat[numeroAssignaturesOptatives];
					optativitat[numeroAssignaturesOptatives]=null;
					numeroAssignaturesOptatives--;
				}
			}

		} else if ((a instanceof Obligatoria)||a instanceof Troncal){
			if (numCredits<4) {
				for (int i = 0; i < obligatorietatMax4; i++) {
					if (obligatorietat[0][i].getCodiAssignatura().equals(codiAssignatura)) {
						obligatorietat[0][i]=obligatorietat[0][obligatorietatMax4];
						obligatorietat[0][obligatorietatMax4]=null;
						obligatorietatMax4--;

					}
				}
			} else if (numCredits>6) {
				for (int i = 0; i < obligatorietatMin6; i++) {
					if (obligatorietat[2][i].getCodiAssignatura().equals(codiAssignatura)) {
						obligatorietat[2][i]=obligatorietat[2][obligatorietatMin6];
						obligatorietat[2][obligatorietatMin6]=null;
						obligatorietatMin6--;
					}
				}
			}else{
				for (int i = 0; i < obligatorietat4a6; i++) {
					if (obligatorietat[1][i].getCodiAssignatura().equals(codiAssignatura)) {
						obligatorietat[1][i]=obligatorietat[1][obligatorietat4a6];
						obligatorietat[1][obligatorietat4a6]=null;
						obligatorietat4a6--;
					}
				}
			}
		}
	}

	/**
	 * Assignatura mes credits.
	 *
	 * @return the assignatura
	 */
	public Assignatura assignaturaMesCredits(){
		Assignatura a = null;
		for (int i = 0; i < obligatorietatMax4; i++) {
			if (a==null|a.getTotal()<obligatorietat[0][i].getTotal()) {
				a=obligatorietat[0][i];
			}
		}
		for (int i = 0; i < obligatorietat4a6; i++) {
			if (a.getTotal()<obligatorietat[1][i].getTotal()) {
				a=obligatorietat[1][i];
			}

		}
		for (int i = 0; i < obligatorietatMin6; i++) {
			if (a.getTotal()<obligatorietat[2][i].getTotal()) {
				a=obligatorietat[2][i];
			}
		}
		for (int i = 0; i < numeroAssignaturesOptatives; i++) {
			if (a.getTotal()<optativitat[i].getTotal()) {
				a=optativitat[i];
			}
		}

		return a;
	}

	/**
	 * Te assignatura.
	 *
	 * @param codi the codi
	 * @return true, if successful
	 */
	public boolean teAssignatura(String codi) {

		for (int i = 0; i < obligatorietatMax4; i++) {
			if (obligatorietat[0][i].getCodiAssignatura().equals(codi)) {
				return true;
			}
		}

		for (int i = 0; i < obligatorietat4a6; i++) {
			if (obligatorietat[1][i].getCodiAssignatura().equals(codi)) {
				return true;
			}
		}
		for (int i = 0; i < obligatorietatMin6; i++) {
			if (obligatorietat[2][i].getCodiAssignatura().equals(codi)) {
				return true;
			}
		}

		for (int i = 0; i < numeroAssignaturesOptatives; i++) {
			if (optativitat[i].getCodiAssignatura().equals(codi)) {
				return true;
			}
		}


		return false;
	}

	/**
	 * Idioma imparticio.
	 *
	 * @param idioma the idioma
	 * @return the int
	 */
	public int idiomaImparticio(String idioma){
		int count=0;
		for (int i = 0; i < numeroAssignaturesOptatives; i++) {
			if (optativitat[i] instanceof  Reglada) {
				if ( ((Reglada) optativitat[i]).getIdioma().equals(idioma)) {
					count++;
				}
			}
		}
		return count;
	}
	
	
	/**
	 * Llistat.
	 */
	public void llistat() {
		for (int i = 0; i < obligatorietatMax4; i++) {
			System.out.println(obligatorietat[0][i].visualitza());
		}

		for (int i = 0; i < obligatorietat4a6; i++) {
			System.out.println(obligatorietat[1][i].visualitza());
		}
		for (int i = 0; i < obligatorietatMin6; i++) {
			System.out.println(obligatorietat[2][i].visualitza());
		}

		for (int i = 0; i < numeroAssignaturesOptatives; i++) {
			System.out.println(optativitat[i].visualitza());
		}
	}

	/**
	 * Llistat.
	 *
	 * @param quina the quina
	 */
	public void llistat(char quina) {
		if (quina=='T') {
			for (int i = 0; i < obligatorietatMax4; i++) {
				System.out.println(obligatorietat[0][i].visualitza());
			}

			for (int i = 0; i < obligatorietat4a6; i++) {
				System.out.println(obligatorietat[1][i].visualitza());
			}
			for (int i = 0; i < obligatorietatMin6; i++) {
				System.out.println(obligatorietat[2][i].visualitza());
			}

		}

		if (quina=='O') {
			for (int i = 0; i < numeroAssignaturesOptatives; i++) {
				System.out.println(optativitat[i].visualitza());
			}
		}


	}

	/**
	 * Llistat.
	 *
	 * @param credit the credit
	 */
	public void llistat(float credit) {
		for (int i = 0; i < obligatorietatMax4; i++) {
			float creditsAssig= obligatorietat[0][i].getTotal();
			if (creditsAssig>=credit) {
				System.out.println(obligatorietat[0][i].visualitza());	
			}

		}

		for (int i = 0; i < obligatorietat4a6; i++) {
			float creditsAssig= obligatorietat[1][i].getTotal();
			if (creditsAssig>=credit) {
				System.out.println(obligatorietat[1][i].visualitza());	
			}		
		}
		for (int i = 0; i < obligatorietatMin6; i++) {
			float creditsAssig= obligatorietat[2][i].getTotal();
			if (creditsAssig>=credit) {
				System.out.println(obligatorietat[2][i].visualitza());	
			}		
		}

		for (int i = 0; i < numeroAssignaturesOptatives; i++) {
			float creditsAssig= optativitat[i].getTotal();
			if (creditsAssig>=credit) {
				System.out.println(optativitat[i].visualitza());
			}
		}
	}

	/**
	 * Calcular import.
	 *
	 * @param matricula the matricula
	 * @param preuCredit the preu credit
	 * @return the float
	 */
	public double calcularImport(String matricula[], float preuCredit){
		double preuMatricula=0;
		for (int i = 0; i < matricula.length; i++) {
			String codi = matricula[i];
			Assignatura a=teAssignatura1(codi);
			if (a.getCodiAssignatura().equals(codi)) {
				if (a instanceof Reconeguda) {
					preuMatricula=preuMatricula+(preuCredit*a.getTotal())-((preuMatricula+(preuCredit*a.getTotal()))*0.75);
				} else {
					preuMatricula=preuMatricula+(preuCredit*a.getTotal());
				}
				
			}

		}
		return preuMatricula;

	}

	/**
	 * Credits optatius.
	 *
	 * @return the float
	 */
	public float creditsOptatius() {
		float creditsoptatius=0;
		for (int i = 0; i < numeroAssignaturesOptatives; i++) {
			creditsoptatius=+optativitat[i].getTotal();
		}

		return creditsoptatius;
	}

	/**
	 * Te assignatura 1.
	 *
	 * @param codi the codi
	 * @return the assignatura
	 */
	private Assignatura teAssignatura1(String codi) {

		for (int i = 0; i < obligatorietatMax4; i++) {
			if (obligatorietat[0][i].getCodiAssignatura().equals(codi)) {
				return obligatorietat[0][i];
			}
		}

		for (int i = 0; i < obligatorietat4a6; i++) {
			if (obligatorietat[1][i].getCodiAssignatura().equals(codi)) {
				return obligatorietat[1][i];
			}
		}
		for (int i = 0; i < obligatorietatMin6; i++) {
			if (obligatorietat[2][i].getCodiAssignatura().equals(codi)) {
				return obligatorietat[2][i];
			}
		}

		for (int i = 0; i < numeroAssignaturesOptatives; i++) {
			if (optativitat[i].getCodiAssignatura().equals(codi)) {
				return optativitat[i];
			}
		}


		return null;
	}
	
	/**
	 * Dades pla estudis.
	 *
	 * @return the int[]
	 */
	public int[] dadesPlaEstudis() {
		int [] dades= new int[8];

		for (int i = 0; i < obligatorietatMax4; i++) {
			int pos=checkPlaEstudis(obligatorietat[0][i]);
			if (pos>=0) {
				dades[pos]++;
			}
		}
		for (int i = 0; i < obligatorietat4a6; i++) {
			int pos=checkPlaEstudis(obligatorietat[0][i]);
			if (pos>=0) {
				dades[pos]++;
			}		}
		for (int i = 0; i < obligatorietatMin6; i++) {
			int pos=checkPlaEstudis(obligatorietat[0][i]);
			if (pos>=0) {
				dades[pos]++;
			}		}

		for (int i = 0; i < numeroAssignaturesOptatives; i++) {
			int pos=checkPlaEstudis(optativitat[i]);
			if (pos>=0) {
				dades[pos]++;
			}		}
		return dades;
	}
	
	/**
	 * Check pla estudis.
	 *
	 * @param a the a
	 * @return the int
	 */
	private int checkPlaEstudis(Assignatura a) {
		String curs = a.getCurs();

		if((a instanceof Obligatoria)||(curs.equals("01"))) {return 0;}
		if((a instanceof Troncal)||(curs.equals("01"))) {return 1;}

		if((a instanceof Obligatoria)||(curs.equals("02"))) {return 2;}
		if((a instanceof Troncal)||(curs.equals("02"))) {return 3;}

		if((a instanceof Obligatoria)||(curs.equals("03"))) {return 4;}
		if((a instanceof Troncal)||(curs.equals("03"))) {return 5;}

		if((a instanceof Obligatoria)||(curs.equals("04"))) {return 6;}
		if((a instanceof Troncal)||(curs.equals("04"))) {return 7;}
		return -1;
	}
	
	/**
	 * Hi ha assignatura.
	 *
	 * @param num the num
	 * @return true, if successful
	 */
	public boolean hiHaAssignatura(float num) {


		for (int i = 0; i < obligatorietatMax4; i++) {
			if ((obligatorietat[0][i].getCreditsTeorics()==num)&&obligatorietat[0][i].getCreditsTeorics()==0) {
				return true;
			}
		}

		for (int i = 0; i < obligatorietat4a6; i++) {
			if ((obligatorietat[1][i].getCreditsTeorics()==num)&&obligatorietat[1][i].getCreditsTeorics()==0) {
				return true;
			}
		}
		for (int i = 0; i < obligatorietatMin6; i++) {
			if ((obligatorietat[2][i].getCreditsTeorics()==num)&&obligatorietat[2][i].getCreditsTeorics()==0) {
				return true;
			}
		}

		for (int i = 0; i < numeroAssignaturesOptatives; i++) {
			if ((optativitat[i].getCreditsTeorics()==num)&&optativitat[i].getCreditsPractics()==0) {
				return true;
			}
		}
		return false;


	}
}
