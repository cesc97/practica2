package sessio3;

/**
 * The Class Obligatoria.
 */
public class Obligatoria extends Assignatura {

/** The compartida. */
private boolean compartida;

/** The nom compatencia. */
private String nomCompatencia;

/**
 * Instantiates a new obligatoria.
 *
 * @param nomAssignatura the nom assignatura
 * @param codiAssignatura the codi assignatura
 * @param creditsTeorics the credits teorics
 * @param creditsPractics the credits practics
 * @param compartida the compartida
 * @param nomCompatencia the nom compatencia
 */
public Obligatoria(String nomAssignatura, String codiAssignatura, int creditsTeorics, int creditsPractics,
		boolean compartida, String nomCompatencia) {
	super(nomAssignatura, codiAssignatura, creditsTeorics, creditsPractics);
	this.compartida = compartida;
	this.nomCompatencia = nomCompatencia;
	super.obligatoriaCount++;
}

/**
 * Instantiates a new obligatoria.
 *
 * @param nomAssignatura the nom assignatura
 * @param codiAssignatura the codi assignatura
 * @param creditsTeorics the credits teorics
 * @param creditsPractics the credits practics
 * @param nomCompatencia the nom compatencia
 */
public Obligatoria(String nomAssignatura, String codiAssignatura, int creditsTeorics, int creditsPractics,
		 String nomCompatencia) {
this(nomAssignatura, codiAssignatura, creditsTeorics, creditsPractics, false, nomCompatencia);
super.obligatoriaCount++;
}

/**
 * Checks if is compartida.
 *
 * @return the compartida
 */
public boolean isCompartida() {
	return compartida;
}

/**
 * Sets the compartida.
 *
 * @param compartida the compartida to set
 */
public void setCompartida(boolean compartida) {
	this.compartida = compartida;
}

/**
 * Gets the nom compatencia.
 *
 * @return the nomCompatencia
 */
public String getNomCompatencia() {
	return nomCompatencia;
}

/**
 * Sets the nom compatencia.
 *
 * @param nomCompatencia the nomCompatencia to set
 */
public void setNomCompatencia(String nomCompatencia) {
	this.nomCompatencia = nomCompatencia;
}

public String visualitza() {
	return super.visualitza()+"\n compartida: "+compartida+" nom compatencia: "+nomCompatencia;

}
public String visualitza(boolean percentatge) {
	return super.visualitza(percentatge)+"\n compartida: "+compartida+" nom compatencia: "+nomCompatencia;
}

}
