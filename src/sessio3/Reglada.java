package sessio3;

/**
 * The Class Reglada.
 */
public class Reglada extends Optativa {

	/** The idioma. */
	private String idioma;
	
	/** The especialitat. */
	private String especialitat;
	
	/**
	 * Instantiates a new reglada.
	 *
	 * @param nomAssignatura the nom assignatura
	 * @param codiAssignatura the codi assignatura
	 * @param creditsTeorics the credits teorics
	 * @param creditsPractics the credits practics
	 * @param idioma the idioma
	 * @param especialitat the especialitat
	 */
	public Reglada(String nomAssignatura, String codiAssignatura, int creditsTeorics, int creditsPractics,
			String idioma, String especialitat) {
		super(nomAssignatura, codiAssignatura, creditsTeorics, creditsPractics);
		this.idioma = idioma;
		this.especialitat = especialitat;
	}
	
	/**
	 * Instantiates a new reglada.
	 *
	 * @param nomAssignatura the nom assignatura
	 * @param codiAssignatura the codi assignatura
	 * @param creditsTeorics the credits teorics
	 * @param creditsPractics the credits practics
	 * @param idioma the idioma
	 */
	public Reglada(String nomAssignatura, String codiAssignatura, int creditsTeorics, int creditsPractics,
			String idioma) {
		this(nomAssignatura, codiAssignatura, creditsTeorics, creditsPractics, idioma,null);
		}

	/**
	 * Gets the idioma.
	 *
	 * @return the idioma
	 */
	public String getIdioma() {
		return idioma;
	}

	/**
	 * Sets the idioma.
	 *
	 * @param idioma the idioma to set
	 */
	public void setIdioma(String idioma) {
		this.idioma = idioma;
	}

	/**
	 * Gets the especialitat.
	 *
	 * @return the especialitat
	 */
	public String getEspecialitat() {
		return especialitat;
	}

	/**
	 * Sets the especialitat.
	 *
	 * @param especialitat the especialitat to set
	 */
	public void setEspecialitat(String especialitat) {
		this.especialitat = especialitat;
	}
	@Override
	public String visualitza() {
	
		return super.visualitza()+"\n idioma: "+idioma+" especialitat: "+especialitat;
	}
	public String visualitza(boolean percentatge) {
		
		return super.visualitza(percentatge)+"\n idioma: "+idioma+" especialitat: "+especialitat;
	}
}
