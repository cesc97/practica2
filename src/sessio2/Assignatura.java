package sessio2;

import java.util.Random;

/**
 * The Class Assignatura.
 */
public class Assignatura {

	/** The nom assignatura. */
	private String nomAssignatura;

	/** The codi assignatura. */
	private String codiAssignatura;

	/** The credits teorics. */
	private int creditsTeorics;

	/** The credits practics. */
	private int creditsPractics;

	/** The petita. */
	public static String petita="petita";

	/** The mitjana. */
	public static String mitjana="mitjana";

	/** The gran. */
	public static String gran="gran";

	/** The troncal. */
	public static String troncal="troncal";

	/** The obligatoria. */
	public static String obligatoria="obligatoria";

	/** The optativa. */
	public static String optativa="optativa"; 

	/** The max 4. */
	private static int max4=0;

	/** The de 4 a 6. */
	private static int de4a6=0;

	/** The min 6. */
	private static int min6=0;
	/**
	 * Instantiates a new assignatura.
	 *
	 * @param nomAssignatura the nom assignatura
	 * @param codiAssignatura the codi assignatura
	 * @param creditsTeorics the credits teorics
	 * @param creditsPractics the credits practics
	 */
	public Assignatura(String nomAssignatura, String codiAssignatura, int creditsTeorics, int creditsPractics) {
		this.nomAssignatura = nomAssignatura;
		this.codiAssignatura = codiAssignatura;
		this.creditsTeorics = creditsTeorics;
		this.creditsPractics = creditsPractics;
	}

	/**
	 * Instantiates a new assignatura.
	 *
	 * @param nomAssignatura the nom assignatura
	 * @param creditsTeorics the credits teorics
	 * @param creditsPractics the credits practics
	 */
	public Assignatura(String nomAssignatura, int creditsTeorics, int creditsPractics) {
		this(nomAssignatura,GenerarCodiAssignatura.generarCodi(),creditsTeorics,creditsPractics);
	}

	/**
	 * Instantiates a new assignatura.
	 *
	 * @param nomAssignatura the nom assignatura
	 * @param creditsTeorics the credits teorics
	 * @param creditsPractics the credits practics
	 * @param tipus the tipus
	 */
	public Assignatura(String nomAssignatura, int creditsTeorics, int creditsPractics,char tipus) {
		this(nomAssignatura,GenerarCodiAssignatura.generarCodi(tipus),creditsTeorics,creditsPractics);
	}

	/**
	 * Instantiates a new assignatura.
	 *
	 * @param nomAssignatura the nom assignatura
	 * @param creditsTeorics the credits teorics
	 * @param creditsPractics the credits practics
	 * @param codi the codi
	 */
	public Assignatura(String nomAssignatura, int creditsTeorics, int creditsPractics, String codi) {
		this(nomAssignatura,GenerarCodiAssignatura.generarCodi(codi),creditsTeorics,creditsPractics);
	}

	/**
	 * Instantiates a new assignatura.
	 *
	 * @param nomAssignatura the nom assignatura
	 * @param midaCredits the mida credits
	 */
	public Assignatura(String nomAssignatura,String midaCredits) {
		Random r = new Random();
		int t=0;
		int p=0;
		int c=0;
		if (midaCredits.equals(gran)) {
			while(c>6)
				t=r.nextInt(7);
			p=r.nextInt(7);
			c=p+t;

		} else 	if (midaCredits.equals(mitjana)) {
			while((c>4)&&(c<6))
				t=r.nextInt(6);
			p=r.nextInt(6);
			c=p+t;

		}else if (midaCredits.equals(petita)) {
			while(c<4)
				t=r.nextInt(5);
			p=r.nextInt(5);
			c=p+t;

		}
		this.nomAssignatura=nomAssignatura;
		this.codiAssignatura=GenerarCodiAssignatura.generarCodi();
		this.creditsPractics=p;
		this.creditsTeorics=t;
	}

	/**
	 * Gets the nom assignatura.
	 *
	 * @return the nomAssignatura
	 */
	public String getNomAssignatura() {
		return nomAssignatura;
	}

	/**
	 * Sets the nom assignatura.
	 *
	 * @param nomAssignatura the nomAssignatura to set
	 */
	public void setNomAssignatura(String nomAssignatura) {
		this.nomAssignatura = nomAssignatura;
	}

	/**
	 * Gets the credits teorics.
	 *
	 * @return the creditsTeorics
	 */
	public int getCreditsTeorics() {
		return creditsTeorics;
	}

	/**
	 * Sets the credits teorics.
	 *
	 * @param creditsTeorics the creditsTeorics to set
	 */
	public void setCreditsTeorics(int creditsTeorics) {
		if(creditsTeorics>0) {
			this.creditsTeorics = creditsTeorics;
		}
	}

	/**
	 * Gets the credits practics.
	 *
	 * @return the creditsPractics
	 */
	public int getCreditsPractics() {
		return creditsPractics;
	}

	/**
	 * Sets the credits practics.
	 *
	 * @param creditsPractics the creditsPractics to set
	 */
	public void setCreditsPractics(int creditsPractics) {
		if (creditsPractics>0) {
			this.creditsPractics = creditsPractics;	
		}

	}

	/**
	 * Gets the codi assignatura.
	 *
	 * @return the codiAssignatura
	 */
	public String getCodiAssignatura() {
		return codiAssignatura;
	}

	/**
	 * Gets the total.
	 *
	 * @return the total
	 */
	public float getTotal() {
		float total=0;

		total = creditsPractics+creditsTeorics;
		return total;

	}

	/**
	 * Percentatge.
	 *
	 * @param teorics the teorics
	 * @return the float
	 */
	public float percentatge(boolean teorics) {
		float percentatge=0;
		float creditsTotals=getTotal();
		float credits=0;
		if (teorics) {
			credits=creditsTeorics;
		} else {
			credits=creditsPractics;
		}
		percentatge=(credits/creditsTotals)*100;
		return percentatge;
	}

	/**
	 * Puja credits.
	 *
	 * @param num the num
	 * @param teorics the teorics
	 */
	public void pujaCredits (int num, boolean teorics) {
		if (teorics) {
			creditsTeorics=+num;
		} else {
			creditsPractics=+num;
		}
	}

	/**
	 * Baixa credits.
	 *
	 * @param num the num
	 * @param teorics the teorics
	 */
	public void baixaCredits (int num, boolean teorics) {
		pujaCredits(-(num), teorics);
	}

	/**
	 * Gets the curs.
	 *
	 * @return the curs
	 */
	public String getCurs() {

		String curs= codiAssignatura.substring(4, 6);
		String c="";
		switch (curs) {
		case "01":
			c= "primer";
			break;
		case "02":
			c= "segon";
			break;
		case "03":
			c= "tercer";
			break;
		case "04":
			c= "quart";
			break;			
		}
		return c;
	}

	/**
	 * Gets the materia.
	 *
	 * @return the materia
	 */
	public  String getMateria() {
		return codiAssignatura.substring(7, 9);
	}

	/**
	 * Gets the codi.
	 *
	 * @return the codi
	 */
	public String getCodi() {
		return codiAssignatura.substring(0, 3);
	}

	/**
	 * Gets the tipus.
	 *
	 * @return the tipus
	 */
	public String getTipus() {
		String tipus= codiAssignatura.substring(10,11);
		String t;
		if (tipus.equals("O")) {
			t=optativa;
		}else if (tipus.equals("B")) {
			t=obligatoria;
		}else {
			t= troncal;
		}
		return t;
	}

	/**
	 * Gets the nom curs.
	 *
	 * @param codi the codi
	 * @return the nom curs
	 */
	public static String getNomCurs(String codi) {
		String curs= codi.substring(4, 6);
		String c="";
		switch (curs) {
		case "01":
			c= "primer";
			break;
		case "02":
			c= "segon";
			break;
		case "03":
			c= "tercer";
			break;
		case "04":
			c= "quart";
			break;			
		}
		return c;
	}

	/**
	 * Gets the curs.
	 *
	 * @param codi the codi
	 * @return the curs
	 */
	public static int getCurs(String codi){
		int curs= Integer.parseInt(codi.substring(4, 6));
		return curs;
	}

	/**
	 * Gets the materia.
	 *
	 * @param codi the codi
	 * @return the materia
	 */
	public static String getMateria(String codi){
		return codi.substring(7, 9);
	}

	/**
	 * Gets the identificador.
	 *
	 * @param codi the codi
	 * @return the identificador
	 */
	public static int getIdentificador(String codi){
		return Integer.parseInt(codi.substring(0, 3));

	}

	/**
	 * Gets the tipus assignatura.
	 *
	 * @param codi the codi
	 * @return the tipus assignatura
	 */
	public static String getTipusAssignatura(String codi){
		String tipus= codi.substring(10,11);
		String t;
		if (tipus.equals("O")) {
			t=optativa;
		}else if (tipus.equals("B")) {
			t=obligatoria;
		}else {
			t= troncal;
		}
		return t;}

	/**
	 * Gets the max 4.
	 *
	 * @return the max4
	 */
	public static int getMax4() {
		return max4;
	}

	/**
	 * Gets the de 4 a 6.
	 *
	 * @return the de4a6
	 */
	public static int getDe4a6() {
		return de4a6;
	}

	/**
	 * Gets the min 6.
	 *
	 * @return the min6
	 */
	public static int getMin6() {
		return min6;
	}


	public String visualitza() {

		return "Assignatura [nomAssignatura=" + nomAssignatura + 
				",\n codiAssignatura=" + codiAssignatura+
				"\n "+getCodi()+" de "+getCurs()+" dins la materia "+ getMateria()+" i "+getTipus()
				+ ",\n creditsTeorics=" + creditsTeorics + " credits que representen un "+percentatge(true)+","+" \n creditsPractics=" + creditsPractics +" credits que representen un "+percentatge(false)+ "]";
	}



}
