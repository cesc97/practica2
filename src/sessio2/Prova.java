package sessio2;


/**
 * The Class Prova.
 */
public class Prova {

	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {

		String codi =GenerarCodiAssignatura.generarCodi();
		System.out.println(codi);
		Assignatura ass1= new Assignatura("intro bd" , "342-01-BD-B", 2, 1);
		Assignatura ass2= new Assignatura("prog poo", 3, 2);
		Assignatura ass3= new Assignatura("web" , 4, 3,'O');
		Assignatura ass4= new Assignatura("iot" , 3,2,"343");
		Assignatura ass6= new Assignatura("UT" ,  4,1,Assignatura.gran);
		System.out.println("metodes assignatura");
		System.out.println(ass1.visualitza());
		ass1.pujaCredits(5, true);
		ass1.baixaCredits(1, false);
		System.out.println(ass1.visualitza());
		System.out.println("metodes grau");
		Grau g = new Grau("informatica", 60, 6, 4);
		g.addAssignatura(ass1);
		g.addAssignatura(ass2);
		g.addAssignatura(ass3);
		g.addAssignatura(ass4);
		g.addAssignatura(ass6);
		g.llistat();
		g.remAssignatura(ass6);
		System.out.println("-------------------------------------");
		System.out.println(g.assignaturaMesCredits().visualitza());
		System.out.println("-------------------------------------");
		g.llistat('T');
		System.out.println("-------------------------------------");
		System.out.println(g.teAssignatura("342-01-BD-B"));
		System.out.println("-------------------------------------");
		g.llistat(5);
		System.out.println("-------------------------------------");
		System.out.println(g.creditsOptatius());
		System.out.println("-------------------------------------");
		String [] matricula = {ass1.getCodiAssignatura(),ass2.getCodiAssignatura(),ass3.getCodiAssignatura(),ass4.getCodiAssignatura()};
		System.out.println(g.calcularImport(matricula, 25F));
	}

}
